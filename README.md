# OpenML dataset: Meta_Album_PLT_NET_Extended

https://www.openml.org/d/44327

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album PlantNet Dataset (Extended)**
***
Meta-Album PlantNet dataset is created by sampling the Pl@ntNet-300k dataset (https://openreview.net/forum?id=eLYinD0TtIt), itself a sampling of the Pl@ntNet Project's repository. The images and labels which enter this database are sourced by citizen botanists from around the world, then confirmed using a weighted reliability score from others users, such that each image has been reviewed by 2.03 citizen botanists on average. Of the 1 081 classes in the original Pl@ntNet-300k dataset, PLT_NET retains the 25 most populous classes, belonging to 21 genera, for a total of 120 688 images total, with min 2 914, max 9 011 image distribution per class. Each image contains a colored 128x128 image of a plant or a piece or a plant from the corresponding class (or in some cases sketches of plants or plant cells on microscope slides), scaled from the initial variable width using the INTER_AREA anti-aliasing filter from Open-CV. Almost all images were initially square; cropping by taking the largest possible square with center at the middle of the initial image was applied otherwise.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/PLT_NET.png)

**Meta Album ID**: PLT.PLT_NET  
**Meta Album URL**: [https://meta-album.github.io/datasets/PLT_NET.html](https://meta-album.github.io/datasets/PLT_NET.html)  
**Domain ID**: PLT  
**Domain Name**: Plants  
**Dataset ID**: PLT_NET  
**Dataset Name**: PlantNet  
**Short Description**: Plants Dataset with different species of plants  
**\# Classes**: 25  
**\# Images**: 120688  
**Keywords**: ecology, plants, plant species  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: Creative Commons Attribution 4.0 International  
**License URL(original data release)**: https://zenodo.org/record/4726653
https://creativecommons.org/licenses/by/4.0/legalcode
 
**License (Meta-Album data release)**: Creative Commons Attribution 4.0 International  
**License URL (Meta-Album data release)**: [https://creativecommons.org/licenses/by/4.0/legalcode](https://creativecommons.org/licenses/by/4.0/legalcode)  

**Source**: PlantNet  
**Source URL**: https://plantnet.org/en/2021/03/30/a-plntnet-dataset-for-machine-learning-researchers/  
  
**Original Author**: Garcin, Camille and Joly, Alexis and Bonnet, Pierre and Lombardo, Jean-Christophe and Affouard, Antoine and Chouet, Mathias and Servajean, Maximilien and Salmon, Joseph and Lorieul, Titouan  
**Original contact**: camille.garcin@inria.fr  

**Meta Album author**: Felix Herron  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@inproceedings{garcin2021plntnetk,
    title={Pl@ntNet-300K: a plant image dataset with high label ambiguity and a long-tailed distribution},
    author={Camille Garcin and alexis joly and Pierre Bonnet and Antoine Affouard and Jean-Christophe Lombardo and Mathias Chouet and Maximilien Servajean and Titouan Lorieul and Joseph Salmon},
    booktitle={Thirty-fifth Conference on Neural Information Processing Systems Datasets and Benchmarks Track (Round 2)},
    year={2021},
    url={https://openreview.net/forum?id=eLYinD0TtIt}
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Micro]](https://www.openml.org/d/44249)  [[Mini]](https://www.openml.org/d/44293)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44327) of an [OpenML dataset](https://www.openml.org/d/44327). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44327/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44327/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44327/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

